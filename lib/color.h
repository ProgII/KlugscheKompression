/// Provide a color datastructures and methods to modify it.

#ifndef COLOR_H
#define COLOR_H

#include <stdio.h>

/// Type for color values (h s v - Format) of a pixel.
typedef struct {

  /// hue value.
  unsigned int h;

  /// satisfaction value.
 double s;

  /// volume value.
 double v;

} color_t;

#endif // COLOR_H
