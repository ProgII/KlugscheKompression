/// Datatypes and functions to store and manipulate the input.

#ifndef INPUT_H
#define INPUT_H

#include "color.h"

/// Forward declaration, definition follows below.
typedef struct input_pixel_t input_pixel_t;

/// Type for an array of input entries that also contains the array size.
typedef struct input_pixel_array_t {

  /// The size of the array.
  unsigned Size;

  /// The actual input pixel array.
  input_pixel_t *Data;

} input_pixel_array_t;

/// Type for a pixel of the input file.
struct input_pixel_t {

  /// The weight of the pixel as defined in the input file.
  unsigned Id;

  /// The color of the pixel as defined in the input file.
  color_t Color;

  /// The array of child entries of this pixel.
  input_pixel_array_t ChildPixels;
};


#endif // INPUT_H
