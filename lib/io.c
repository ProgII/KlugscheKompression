#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "io.h"
#include "input.h"

void freeRecursively(input_pixel_array_t* Pixels){
	//TODO
	return;
}

int readId(FILE* File, unsigned* id) {
	// Read an integer from the file stream and store the value in id.
	int NumParametersRead = fscanf(File, "%u", id);  
	// Check that we actually read an integer.
	return NumParametersRead == 1;
}

int readColor(FILE* File, color_t* Color) {
	(void)Color;
	(void)File;
	
  	//TODO implement this
 
  	return 0 ;
}




int readPixelsRecursively(FILE* File, input_pixel_array_t Pixels) {
	for (unsigned i = 0; i < Pixels.Size; i++) {
	    	input_pixel_t* Pixel = &Pixels.Data[i];
			
	    	if (!readId(File, &Pixel->Id)) {
	      	    fputs("Couldn't read weight\n", stderr);
	      	    return 0;
    		}
    
  		//TODO implement this
  	}    
  	return 0;
}


input_pixel_array_t readPixels(const char* Filename) {
	input_pixel_array_t Pixels= {0, NULL};
   
  	//TODO implement this
  	
	return Pixels;
 }

 
int main (int argc, char** argv){
	if(argc < 2){
        	fputs("You forgot the input file\n", stderr);
    	}
	input_pixel_array_t pixels = readPixels(argv[1]);
	printf(" hi %d",pixels.Size);
}
  
