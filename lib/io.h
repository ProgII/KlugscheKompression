/// Provide input/output operations.

#ifndef IO_H
#define IO_H

#include "color.h"
#include "input.h"

#include <stdio.h>

/// Read a id value from @p File and return it via @p id.
///
/// @returns: True, if and only if the id was actually read.
int readId(FILE *File, unsigned *id);

/// Read a color value from @p File and return it via @p Color.
///
/// @returns: True, if and only if the color was actually read.
int readColor(FILE *File, color_t *Color);

/// Read all input pixels from @p File and return them via @p Pixels.
///
/// @returns: True, if and only if there was no reading error.
int readPixelsRecursively(FILE *File, input_pixel_array_t Pixels);

/// Read an array of Pixels from the file named @p Filename.
///
/// If the file does not exist or its content violates the Pixel format
/// the function shall return an empty input pixel array (size == 0) with
/// null as data pointer. The function should never leave any open files or
/// non-accessible memory behind!
input_pixel_array_t readPixels(const char *Filename);


//This function frees the Data of the pixels.

void freeRecursively(input_pixel_array_t *Pixel);




#endif // IO_H
